
All the additional benefits listed below are available for full time employees. They are not valid for external contractors.

# Contract and prize rules

(This section is not valid for consultants and contractors)

1. Type of contract: “CCNL terziario commercio”, 14 monthly salaries, permanent contract.

2. PDR: supplementary 2nd level contract specific for “Work life balance” specific for Welfare benefits provided by the company.

3. Fondo Est (Health fund provided by CCNL).


4. Company Prize (valid just for permanent employees):
    - This prize is a gross amount equivalent to the 8% of the RAL; it shall be issued on the basis of the results of the company ( Ex. EBITDA )
    - The company result will be defined, at the beginning of the year, by a minimum value and maximum value. This target will be defined having in mind to have a good probability to reach at least 50% of this range. If we are under the mid value we are underperforming but still acceptable if above the minimum, if we are above 50% we are over performing.
    - The individual prize will be a linear percentage between the minimum and maximum value. Es. If the fork is between 1M and 2M of EBITDA, if at the end of the year we reach 1M the prize is 0, if we reach 1.5M the prize is 50%, if we reach 2M the prize will be 100%. The prize percentage is referred to the gross amount equivalent to the 8% of the RAL.
    - The prize shall be paid in two steps:
        - First tranche: 50% in April year X+1 (year X is the year for calculation reference)
        - Second tranche: 50% in May year X+1 (year X is the year for calculation reference)
    - An employee hired before the 31st of August shall be eligible for the company prize but proportionally to the effective months; an employee hired after the 31st of August shall not be eligible for this prize
    - In the eligibility period:
        - If an employee will be hired before the 15th of the month shall be eligible starting from the same month;
        - If the employee will be hired after the 15th of the month shall be eligible starting from the next month (i.e. if hired 15th of March shall earn 9/12 of the jackpot amount).

5. Resignation policy:
    - If the resignation comes before the issuing dates of the Company Prize in the year X, the employee will lose the right to the prize relative to the X-1 year. The employee shall remain eligible for the second and third tranche of the Performance Prize
    - If the resignation comes before June the 30th of the year X, the employee shall not be eligible for the first tranche of the Performance Prize and for the Company Prize related to year X
    - If the resignation comes before December the 31st of the year X, the employee shall be eligible just for the first tranche of the Performance Prize and not for the rest.

6. Referral fee: the employee that suggests a person that will be hired successfully, shall be prized with € 1.000 gross value that shall be paid when the trial period of the neo-hired person will be over. We cannot hire people from all companies, please take a look at the recruiting denylist before suggesting a candidate. The list is in `BigData/HR/denylist recruiting.xlsx`.

7. Welcome Pack (received when a new employee comes to the office): a notebook, an additional screen, board, mouse, port adapter and headset.
   {% role %}People Operations/Welcome{% endrole %} proposes to each new employee models of hardware and accessories selected by `circle:PeopleOperations` according to the budget and technical quality defined.
The employee, according to his needs, can propose a different model than the one proposed by {% role %}People Operations/Welcome{% endrole %} only for board, mouse, port adapter and headset: the expense must still be within the approved budget and, in this case, the employee assumes responsibility for the material chosen.

8. No badge, no hour count: in Agile Lab none will check the effective worked hours during the day; the employee shall confirm the daily work and the trip expenses made for the company on the Elapseit platform: no expenses form is requested to the employee, just proof of purchases.

9. The extra hours during the night or the weekend shall be paid as stated by the CCNL. Availability service shall be paid as agreed by the extra-timetable service gross values.

10.	Extra prizes shall be paid when a group of employees is requested for extra working effort in a specific delivery or project.

11.	`(20-x)*7€` meal vouchers per month shall be distributed to the employees. `x` are holidays + sick days. (available only in Italy)

12.	An additional Health policy is provided to the employee (in charge to Agile Lab, no fiscal ownership for the employee) by a specific agreement with Previgest (Gruppo Generali Assicurazioni): no ticket, direct coverage, no cash in advance for the majority of the services for therapies, medical examinations - and so on - with the benefit of a dedicated booking service with zero wait queues. The service is dedicated just to the employee who can extend the coverage to his/her family paying the amount difference. Check [Health Insurance](HealthInsurance.md) for more details. (available only in Italy)

13.	Additional Welfare conversion: possibility to “convert” the net amount of the prizes in services or products purchase with a +30% price enhancement (i.e. it’s possible to purchase a product that costs 130 € with 100 € net prize amount exchange).

# Training Budget

Each person, including external contractors/consultants/freelance (__as long as they work full time for AgileLab__), has a budget that can spend along the year. It is composed by:
* **Cash Budget**: 1000 Eur/year
* **Time Budget**: 12 hours/month

> **NOTE**: while cash budget is a per year amount, so you can even spend it all on December 31st, time budget is per month. In order to allow the management to correctly forecast budget and people allocation, while at the same time motivating and allowing employees not to waste their time budget if they are full on their projects for a few weeks/months, a **time budget consumption policy** is to be considered: *time budget can be accumulated up until a 3 months span (with monthly budget being fully unlocked at the beginning of the month), the time not spent learning in the previous months is unfortunately wasted.*
>
> For example, if you don't take any time to learn something new for the first 4 full months of the year (4 * 12 = 48 hours), on May 1st you'd "only" have left the previous 2 months window worth of time budget (March, April) which adds-up with May's budget making it a total of 3 * 12 = 36h spendable in May. Please, remember that even in this case it would mean being off projects for almost 1 whole week, which would complicate things with customers probably, so use your own sensitivity to understand what makes sense or not.
> 
> A tool to facilitate personal training time and budget tracking is available in company's `sharepoint://BigData/Documents/Self Management/TrainingBudgetTemplate.xlsx`

All the guidelines to be followed for time tracking on Elapseit are available in [Project Time Tracking](ProjectTimesheet.md) (focus on the Training section).

Here is a list of how you can spend such budget:

### Conferences 
Conferences are a great resource to feed the technical growth and to stimulate the desire to try new technologies or to implement new solutions. The comparison with what others do is always positive.

You can attend one international (in Europe) conference per year. The conference must be inherent to the spectrum of technologies and interests of Agile Lab.
Agile Lab will pay for:
- Trasportations
- Accomodation (max 3 nights)
- Conference Ticket
- Food (max 2 days)

All of them must be less than "Cash Budget" + 500 Eur (for travel expenses)

**Note**: the extra budget for travelling and accomodation of 500 Eur is to be intended as an extra bonus which doesn't add up to the standard budget. This means that, for example, if you spend 100 Eur in travel expenses you DON'T have 400 Eur as extra personal training budget. 
Another example: let's say the conference ticket costs 1000 Eur and as travel expenses you spend 100 Eur: in this case you don't have any other training budget left, since the 1000 Eur for the conference ticket consumed all of it. There's no "automatic conversion" of travel expenses budget to regular training budget, since this would be unfair w/r/t/ colleagues who don't attend a conference (thus having "only" 1000 Eur budget).

The days spent at the conference are subtracted by the "Time Budget".
Check [Conferences](Conferences.md) to see how to attend a conference.

### Training material
With the "Cash Budget", you can buy courses, books and other materials useful for certifications or your professional path.

In the case of subscriptions, People Operations will make sure that the automatic renewal of the purchased service is not activated. 

### Learning
The "Time Budget" is also useful to attend internal workshops or online courses on our internal LMS. It is also allowed to spend your "Time Budget" to read books, follow bought courses, etc.

### Extra time to study for certifications
You can decide to use the "Time Budget" to gain extra time to study in preparation for a certification (see [Certifications](Certifications.md) for details about certification benefit).

### Outsourced Technical Trainings

Another alternative are outsourced trainings, they are good compromises for those who are looking for a more effective (learn-by-doing) approach, but with tool boxes mostly full of I-shaped contents.

Such training must be inherent to the spectrum of technologies and interests of Agile Lab or related on foundational technical approaches at least.

Limitations are the same applied for conferences. [Outsource Trainings](OutsourcedTechnicalTrainings.md) for details.

**NOTE**: For guidelines about how to make/request purchases or reimbursements, please consult [Spending Company Money](SpendingCompanyMoney.md). 

# Certifications

IT certifications are another great way to demonstrate our excellence in a specific field, so employees are encouraged to do them as much as possible. If you follow the process described in the [Certifications](Certifications.md) section, Agile Lab will cover all the costs.



# Open Source Contributing

In Agile Lab we want to let people express their potential contributing to world class open source projects. Please check [how it works](OpenSource.md).




# Smart/Remote Working

In Agile Lab we always try to put people first, we strongly believe in the energy that they release when they are in the best mental and physical conditions possible. We also believe that social relationships and cultural values are important to reach a good work-life balance, and our offices are the best places to enable these values and to introduce new people to the company culture. Don't forget that the junior colleagues can learn from senior ones to build attitudes and behaviors that are important in work but in life too.

This produces value for the people, and people produce value for the company.

Whatever the situation will evolve after the Covid-19 pandemic, Agile Lab will not become a fully remote company but it will let you choose to live the full-remote experience or not. Happy people mean happy company, and the result is what matters to us (as usual).

Everyone can access two different kinds of contracts:

**Full Remote**
- You need to communicate formally the domiciles from where you intend to work
- You can work from all the Agile Lab offices whenever you want but we do not guarantee you'll find an empty desk for you
- You have to be available for business trips

**Smart Working**
- We guarantee you your desk in one of the Agile Lab offices
- You have to spend at least the 40% of your effective working days from the office in one month (also in August). Long working from home period should be approved by the team leader and by the customers
- You have to be available for business trips

You can jump from one contract to the other once every 12 months, your compensation won't change.

We strongly suggest waiting until the end of the pandemic to make your choice.



