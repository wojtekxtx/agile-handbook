# Project Software Quality and Practices

Agile Lab is eager to provide its customers with high quality software.
<br/>Very often customers adopt their own practices that do not completely fit with our internal agile workflow.
<br/>This is expected. Every organization embracing agile practices will interpret them depending on their own real scenarios and needs.
<br/>Also, there are still development projects running with different methodologies than agile.

For this reason, we have built a list of criteria to monitor the maturity of agile practices and in turn the software quality.
<br/> This check list is a sort of maturity model that we apply at project level to create alignment and plan for improvements.

## Practices
* **Unit testing**: Testing features related to one or many components belonging to a single deployment cycle.
* **Integration Testing**: Testing integration between one or many components belonging to a single deployment cycle against components released by independent deployment cycles.
* **Code review**:	One or more code reviewers in charge to detect design issues, check existing tests and detect missing ones, recommend good development practices.
* **Software design**: A score determined by the [Quality Cop](QualityCop.md) on the basis of our [Software Development Guidelines](SoftwareDevelopmentGuidelines.md)
* **System architecture**:	A score given by the [Architect](Architecture.md) during the last HLD review (see [Workflow](Workflow.md) for more details).
* **Performance testing**:	Testing speed, load, scalability, reliability. <br/>A performance test is designed on the basis of the specific use-case and requires a production-like environment.
* **System testing**: An E2E test of the application within its running system.
* **Resiliency testing**: Testing high-availability or fault-tolerance.
* **Continuous integration**: Automated build and test cycles for each code commit
* **Continuous delivery**: This includes continuous integration and supports the automated delivery of a software component to an artifact repository
* **Continuous deployment**: Deployment of changes to production without manual intervention. This includes continuous integration.
* **Automated performance test**: Automation of performance tests.
<br/>Performance test must be repeatable.
* **Automated system test**: Automation of e2e test. E2E test must be repeatable.
* **Continuous testing**: Automated unit, integration and system test within the same deployment pipeline. 
* **Chaos engineering**: Automated and randomic resiliency tests of a production live system.

## Estimation
For each item `practice-i`, project members assign the percentage `Score_i` of how that practice is adopted and effective in the project.

For instance, if in a project we have the following code coverage measures:
```
CodeCoverage(Prj1) = 40%
CodeCoverage(Prj2) = 50%
CodeCoverage(Prj3) = 50%
CodeCoverage(Prj4) = 20%
```

We can estimate a percentage score of 40% for the unit testing practice:
```
Score Unit Testing => (40%+50%+50%+20%)/4 = 40% 
```

If code coverage is not available, every project should do any other fair evaluation.
Of course, in this case it is not expected to have high scores for unit testing.

The whole estimation is considered a self-assessment. This is not going to be used to compare different projects, since they are supposed to be different and not comparable.
<br/>It is very important to provide truthful evaluation because this will trigger actions to improve.
<br/>Anyway, we want to establish common criteria and share same values in theory and practice across all projects.

## Metric computation

The computation of the metric related to the software quality within a project proceeds as follows.

Given this list of practices, we associate two scores for each item:
* Positive weight `p_i`: This is a score between 0 (not expected at all) to 1 (always expected) setting the importance of this practice
* Negative weight `n_i`: This is a score between -1 (not applied) to 0 (fully committed) setting the penalty in quality due to not adopting this practice

The final metric is computed as follows and take values in the range `-1 <= SoftwareQuality <= 1`:
```
SoftwareQuality = Sum(Score_i*p_i)/Sum(p_i) + Sum((1-Score_i)*n_i)/Sum(n_i)
```

Project Leads and References are  provided with a spreadsheet that contains a software metric calculator where these formulas are implemented and only scores are necessary. 
