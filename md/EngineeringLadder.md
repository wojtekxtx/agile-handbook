The Engineering Ladder is our way to provide people a professional growth path. 
What is needed to advance to the next level and get a promotion is well explained and explicit (transparency).

Our Path is composed of seven levels. The first four are the same for everyone, while the last three are different for tech and manager tracks, as shown in the below image.

![Ladder](images/ladder.png)

The Engineering ladder is not related to self-management roles. Self-management roles are needed by the organization, while each individual requires ladder levels to grow as a professional. It helps to develop skills across four main directions:
* Dex - Technical skills
* Str - Get Stuff Done
* Wis - Impact
* Cha - Communication & Leadership

The extended document is located in BigData Sharepoint / Career Ladder and it will be updated and releases every six months by the `role:Engine/COO`

This ladder was built taking inspiration mainly from the "rent the runaway" ladder and many others. Thanks



### Rules

* Expectations at each level are cumulative
*  Dex and Str ( the «adder» attributes) are the primary drivers of promotions at lower levels. At higher levels, the 
importance gradually shifts to Wis and Cha ( the «multiplier» attributes )
• Not everyone will progress in each column in lockstep. If you are Eng4 for Dex and Eng2 for Cha, you will not be stuck at Eng2, but you will likely not be promoted all the way to Eng4.  Higher levels requires full compliance. 
• To advance to the next level, you need to consistently perform at that level for some time (6M – 1Y) and then 
needed by the organization
* To reach Engineer 3, a positive experience as Project/Team leader is helpful
* To reach Engineer 4 or above, an experience as project/team leader is almost mandatory
* Each level has a compensation range associated. If you are stuck in a level, you can anyway move forward your compensation, but only up to the upper limit.
* Each level has a quota, calculated by the engine lead link based on the total number of people in the Engine department and their distribution across levels.
* Ladder levels are not applied to external consultants hired by other companies. Those instead we hire remotely are included in this mechanism.
* Not all the people can be promoted to the higher levels.
* For higher levels, it is not sufficient to demonstrate skill but is necessary to act consistently, generating a tangible impact. Very often is not possible to create such an impact without having the possibility to serve in specific roles. This is the motivation for level quotas because we don't want people doing activities for ladder climbing and not for the company's needs.



### How to get promoted

* When is possible we approach things to bottom-up and with self-responsibility in mind
* If you want to be promoted, you need to assess yourself and convince your coach ( project leader/lead link ) that you are ready
* Ask your coach to assess with Engine lead link if there is quota into the next level and then present your candidacy sending an email to the upper lead link. The Engine lead link will prioritize in case of multiple application for a level that has no enough capacity. Within the candidacy you can attach your brag file or other kind of documentation that is demonstrating your target level.
* Your coach  will mentor you along the process, helping to build the brag file to support your pitch 
* Pitch will be done to a specific committee.
* if your mentor is not coaching you enough, please keep him/her accountable

The coach has a huge responsibility in this process:
- if a candidate is not ready to apply for the next level and the coach does not detect it, it can cause a huge demotivation effect in case it is not promoted
- If a candidate is ready, but is not self-confident to apply for the promotion, could be demotivating as well because no-one is recognizing its value
- Coach must be super transparent and should always coach to reach the goal, but not to sponsor a not ready candidate


### When to get promoted

The candidate can apply to pitch at the beginning of each quarter within a two weeks window, and the candidacy must be presented at least one month before. 
For example if you want to pitch at the beginning of Aprile ( End Q1), the application must be done before the end of february.


### The brag file

Not all the people in the committee are aware of what the candidate is doing and how she/he is performing. The ladder is composed by a list of expectations that the candidate needs to fulfill. Most of them are soft or not easy to demonstrate in an interview, so the candidate should build a brag file with its major achievements and how these are fulfilling the ladder requirements. It is also a matter of transparency.
The brag file should be a sharable document and it must be shared with the committee at least one week before the pitch, so they can prepare questions and can have a clear overview about it.
The document should be self-explanatory and should demonstrate that candidate is acting at the target level since 6-12 months with consistency.
Especially for higher levels, it is crucial to focus more on impact and outcomes than actions and self-referential items. For example, don't report "I created a new process for code reviews" without mentioning the positive impact on code review effectiveness and the level of adoption of this new practice within the company.
Because not all the ladder requirements can be documented the coach can certify for the candidate that he/she is compliant ( possibly with a cover letter ) or the committe can decide to ask questions about a specific topic to properly assess it. 



### Promotion Committee

We want to distribute the accountability and don't let a single person judge for a promotion, limiting the judgment bias and the candidate's frustration against a single person.

The following roles will compose the committee:
- The lead link of the elevator plus two other components of the elevator (at least Eng 4.)
- The candidate's coach (project leader/lead link)
- the lead link of the candidate's coach (upper lead link)
- 2 team members ( or former team members) of the candidate, chosen by the him/her. ( if you are not in the position to pick up two team members, explain why and proceed anyway)


### The Pitch

The pitch is a meeting among peers, it is not an interview but the committe will ask questions about the brag file or will assess open points. The pitch and the brag file should focus only on expectations points that coach or the committe think must be proven.

After the pitch of the candidate, the committee will express an evaluation in the following way:
* Technical Skills: coach + people from the elevator circle
* Get Stuff Done: coach + team members
* Impact: coach + upper lead link
* Communication & Leadership: coach + upper lead link + team member

To get the promotion on each feature, you need to reach unanimity and the result will be given immediately after the pitch.

If you get not promoted, you will receive extended feedback about motivations and what you should focus on to get there.


### Compensation

The compensation must always be decided by a person that is possibly two levels ( of the ladder) above the promotion's candidate's target level.


